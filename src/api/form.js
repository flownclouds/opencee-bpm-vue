import request from '@/utils/request'

/**
 * 状态
 */
export const statusMap =
    { '1': '启用',
      '0': '禁用'
    }

/**
 * 分类流程列表
 * @param params
 */
export const listCategoryForm = (params) => request({
  url: '/bpm/form/listCategoryForm',
  method: 'get',
  params
})

