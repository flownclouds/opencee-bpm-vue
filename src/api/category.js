import request from '@/utils/request'

/**
 * 状态
 */
export const statusMap =
    { '1': '启用',
      '0': '禁用'
    }

/**
 * 列表页
 * @param params
 */
export const page = (params) => request({
  url: '/bpm/category/page',
  method: 'get',
  params
})

export const remove = (data) => request({
  url: '/bpm/category/remove',
  method: 'post',
  data
})

export const getById = (params) => request({
  url: '/bpm/category/getById',
  method: 'get',
  params
})

export const list = (params) => request({
  url: '/bpm/category/list',
  method: 'get',
  params
})

export const save = (data) => request({
  headers: {
    'Content-Type': 'application/json'
  },
  url: '/bpm/category/save',
  method: 'post',
  data
})
