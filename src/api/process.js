import request from '@/utils/request'

/**
 * 状态
 */
export const statusMap =
    { '1': '启用',
      '0': '禁用'
    }

/**
 * 列表页
 * @param params
 */
export const page = (params) => request({
  url: '/bpm/process/page',
  method: 'get',
  params
})

export const remove = (data) => request({
  url: '/bpm/process/remove',
  method: 'post',
  data
})

export const importFile = (data) => request({
  headers: {
    'Content-Type': 'multipart/form-data'
  },
  url: '/bpm/process/import',
  method: 'post',
  data
})

export const updateState = (data) => request({
  url: '/bpm/process/update-state',
  method: 'post',
  data
})

export const pageHistory = (params) => request({
  url: '/bpm/process/history/page',
  method: 'get',
  params
})

export const removeHistory = (data) => request({
  url: '/bpm/process/history/remove',
  method: 'post',
  data: data
}
)

export const pageRunning = (params) => request({
  url: '/bpm/process/running/page',
  method: 'get',
  params
})

export const updateRunningState = (data) => request({
  url: '/bpm/process/running/update-state',
  method: 'post',
  data
})

export const removeRunning = (data) => request({
  url: '/bpm/process/running/remove',
  method: 'post',
  data
})

export const getUserTaskList = (params) => request({
  url: '/bpm/process/getUserTaskList',
  method: 'get',
  params
})

export const getStartForm = (params) => request({
  url: '/bpm/form/getStartForm',
  method: 'get',
  params
})
