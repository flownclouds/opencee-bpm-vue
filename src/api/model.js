import request from '@/utils/request'

/**
 * 状态
 */
export const statusMap =
    { '1': '启用',
      '0': '禁用'
    }

/**
 * 列表页
 * @param params
 */
export const page = (params) => request({
  url: '/bpm/model/page',
  method: 'get',
  params
})

export const remove = (data) => request({
  url: '/bpm/model/remove',
  method: 'post',
  data: data
}
)

export const save = (data) => request({
  url: '/bpm/model/save',
  method: 'post',
  data
})

export const saveModelEditor = (data) => request({
  url: '/bpm/model/editor/save',
  method: 'post',
  data
})

export const deploy = (data) => request({
  url: '/bpm/model/deploy',
  method: 'post',
  data
})

export const info = (params) => request({
  url: '/bpm/model/info',
  method: 'get',
  params
})
