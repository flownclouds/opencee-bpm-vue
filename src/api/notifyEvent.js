import request from '@/utils/request'

/**
 * 状态
 */
export const statusMap =
    { '1': '启用',
      '0': '禁用'
    }

/**
 * 列表页
 * @param params
 */
export const page = (params) => request({
  url: '/bpm/notify-event/page',
  method: 'get',
  params
})

export const remove = (data) => request({
  url: '/bpm/notify-event/remove',
  method: 'post',
  data: data
}
)

export const save = (data) => request({
  headers: {
    'Content-Type': 'application/json'
  },
  url: '/bpm/notify-event/save',
  method: 'post',
  data
})

