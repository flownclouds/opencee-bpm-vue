import request from '@/utils/request'

/**
 * 状态
 */
export const statusMap =
  { '1': '启用',
    '0': '禁用'
  }

/**
 * 开发者类型
 */
export const developerTypeMap =
    { '0': '企业自有',
      '1': '第三方企业',
      '2': '第三方个人'
    }

/**
 * 应用类型 server-服务应用 app-手机应用 pc-PC网页应用 wap-手机网页应用
 */
export const appTypeMap =
    { 'server': '服务应用',
      'app': '手机应用',
      'pc': 'PC网页应用',
      'wap': '手机网页应用'
    }

/**
 * 列表页
 * @param params
 */
export const getPage = (params) => request({
  url: '/bpm/application/page',
  method: 'get',
  params
})

export const list = (params) => request({
  url: '/bpm/application/list',
  method: 'get',
  params
})

export const remove = (data) => request({
  url: '/bpm/application/remove',
  method: 'post',
  data: data
}
)

export const save = (data) => request({
  headers: {
    'Content-Type': 'application/json'
  },
  url: '/bpm/application/save',
  method: 'post',
  data
})

