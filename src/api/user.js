import request from '@/utils/request'

export function getInfo() {
  return request({
    url: '/base/user/my',
    method: 'get',
    params: { sid: process.env.VUE_APP_SID }
  })
}

export function logout() {
  return request({
    url: `${process.env.VUE_APP_OAUTH2_LOGOUT_URL}`,
    method: 'get'
  })
}

export const upload = (data) =>
  request({
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    url: '/base/sys/upload',
    method: 'post',
    data
  })
