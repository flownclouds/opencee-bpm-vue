import CustomRenderer from './custom-renderer'

export default {
  __init__: ['customRenderer'],
  customRenderer: ['type', CustomRenderer]
}
