import Vue from 'vue'
import BpmnDesigner from './BpmnDesigner'
import ProcessDesigner from './BpmnDesigner/ProcessDesigner/ProcessDesigner.vue'
import PropertiesPanel from './BpmnDesigner/ProcessPanel/ProcessPanel.vue'
import ProcessPanel from './BpmnDesigner/ProcessPanel/ProcessPanel.vue'
import KFormDesign from './FormDesign/index'
import Cmp from './FormDesign/CustomComponent/index.vue'
import PageModal from '@/components/PageModal/index'

// 导入样式
import './FormDesign/styles/form-design.less'

import TableOperations from '@/components/TableOperations/index'

const components = [
  BpmnDesigner,
  ProcessDesigner,
  PropertiesPanel,
  ProcessPanel,
  TableOperations,
  PageModal
]

components.forEach((component) => {
  Vue.component(component.name, component)
})

Vue.use(KFormDesign)
