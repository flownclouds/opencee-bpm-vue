import CustomRules from './custom-rules'

export default {
  __init__: ['customRules'],
  customRules: ['type', CustomRules]
}
